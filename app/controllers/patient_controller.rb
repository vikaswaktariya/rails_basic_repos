class PatientController < ApplicationController
 # before_action :add_page
 # before_filter :view_page
  def index
    redirect_to view_page_patient_index_url
  end

  def view_page
    flash[:notice] = "Welcome to view page"
    @pateint_info = Patient.all

    #@pateint_info = Patient.all
    #@pateint_info = Patient.take(2) #fetch 2 data from DB
    #@pateint_info = Patient.find([8,9]) #set limit for all data
    #@pateint_info = Patient.first(3) #fetch first 3 data
    #@pateint_info = Patient.order(:text).last(3) #fetch by order use for DESC='last' and ASC by default
    #@pateint_info = Patient.where(title:'reter',id:21) #fetch by condition
  end

  def view_single_info
    @every_pateint_info = Patient.find(params[:id])
  end

  def add_page

  end

  def edit_page
    @patient_update = Patient.find(params[:id])
  end

  def save_pateint
    @patient_save = Patient.new(patient_permission)

    if @patient_save.save
      flash[:info] = "Save successfully"
      redirect_to view_page_patient_index_url
    end

  end

  def update_pateint
    @patient_update = Patient.find(params[:id])
    if @patient_update.update(patient_permission)
     flash[:info] = "Update successfully"
     redirect_to view_page_patient_index_url
    end

  end

  def delete_pateint
    @delete_pateint = Patient.find(params[:id])
    @delete_pateint.destroy

    flash[:info] = "Information successfully deleted..."
    redirect_to view_page_patient_index_url
  end

  private
    def patient_permission
      params.require(:patient).permit(:name, :address, :problem)
    end
end
