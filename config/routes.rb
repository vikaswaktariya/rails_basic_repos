Rails.application.routes.draw do
  root "patient#index"

 # namespace :directory_name do
  #  resources :controller1, :controller2
 # end

 # scope :module => "admin" do
 #   resources :controller1, :controller2
 # end

  # resources :patient, only: [:post]
  # resources :patient, except: [:show]


  resources :patient, :except => [ :new, :create, :edit, :update, :show, :destroy] do

    member do #member require id
      get :view_single_info
      get :edit_page
      get :delete_pateint
      patch :update_pateint
    end

    collection do #collection does't require any id
      get :view_page
      get :add_page
      post :save_pateint
    end
  end
=begin
  resources :doctor, :except => [ :new, :create, :edit, :update, :show, :destroy] do

    member do #member require id
      get :view_single_info
      get :edit_page
      get :delete_pateint
      patch :update_pateint
    end

    collection do #collection does't require any id
      get :view_page
      get :add_page
      post :save_pateint
    end
  end
=end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
