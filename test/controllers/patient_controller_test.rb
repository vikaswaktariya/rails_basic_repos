require 'test_helper'

class PatientControllerTest < ActionDispatch::IntegrationTest
  test "should get view_page" do
    get patient_view_page_url
    assert_response :success
  end

  test "should get add_page" do
    get patient_add_page_url
    assert_response :success
  end

end
